Rails.application.routes.draw do
  root 				'up_arise#Home'
  get 'about'	=>	'up_arise#About'
  get 'members'	=>	'up_arise#Members'
  get 'events'	=>	'up_arise#Events'
  get 'contact'	=>	'up_arise#Contact'
end
